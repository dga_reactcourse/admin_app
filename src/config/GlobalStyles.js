import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
  html, body, #root {
    font-family: ${props => props.theme.fontFamily}
  }
`
